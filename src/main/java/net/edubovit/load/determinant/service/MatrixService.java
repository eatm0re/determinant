package net.edubovit.load.determinant.service;

import net.edubovit.load.determinant.repository.MatrixRepository;
import net.edubovit.load.determinant.domain.Matrix;
import net.edubovit.load.determinant.mapper.MatrixMapper;
import net.edubovit.load.determinant.util.MatrixUtils;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class MatrixService {

    @Value("${determinant.matrix-size}")
    private int matrixSize;

    private final MatrixRepository matrixRepository;
    private final MatrixUtils matrixUtils;
    private final MatrixMapper matrixMapper;

    @Transactional
    public int[][] getMatrixBySeed(long seed) {
        return retrieveOrGenerate(seed).getMatrixRaw();
    }

    @Transactional
    public long getDeterminantBySeed(long seed) {
        return retrieveOrGenerate(seed).getDeterminant();
    }

    @Transactional
    public int[][] generateMatrixBySeed(long seed) {
        return generate(seed).getMatrixRaw();
    }

    private Matrix retrieveOrGenerate(long seed) {
        var result = matrixRepository.findBySeed(seed);
        if (result == null) {
            result = generate(seed);
        } else {
            result.setMatrixRaw(matrixMapper.mapMatrix(result));
        }
        return result;
    }

    private Matrix generate(long seed) {
        int[][] matrixRaw = matrixUtils.generateMatrix(matrixSize, seed);
        var result = matrixMapper.mapMatrix(matrixRaw);
        result.setMatrixRaw(matrixRaw);
        result.setSeed(seed);
        result.setDeterminant(matrixUtils.calculateDeterminant(matrixRaw));
        matrixRepository.save(result);
        return result;
    }

}
