package net.edubovit.load.determinant.util;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class MatrixUtils {

    public int[][] generateMatrix(int n, long seed) {
        int[][] matrix = new int[n][n];
        var random = new Random(seed);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = random.nextInt();
            }
        }
        return matrix;
    }

    public long calculateDeterminant(int[][] matrix) {
        if (matrix.length == 1) {
            return matrix[0][0];
        }
        long sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            long detMinor = matrix[0][i] * calculateDeterminant(getMinor(matrix, i));
            if (i % 2 == 0) {
                sum += detMinor;
            } else {
                sum -= detMinor;
            }
        }
        return sum;
    }

    private int[][] getMinor(int[][] matrix, int minorNumber) {
        int[][] result = new int[matrix.length - 1][matrix.length - 1];
        for (int i = 1; i < matrix.length; i++) {
            if (minorNumber >= 0) {
                System.arraycopy(matrix[i], 0,
                        result[i - 1], 0,
                        minorNumber);
            }
            if (matrix[i].length - minorNumber + 1 >= 0) {
                System.arraycopy(matrix[i], minorNumber + 1,
                        result[i - 1], minorNumber,
                        matrix[i].length - minorNumber - 1);
            }
        }
        return result;
    }

}
