package net.edubovit.load.determinant.mapper;

import net.edubovit.load.determinant.domain.Matrix;
import net.edubovit.load.determinant.domain.MatrixRow;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class MatrixMapper {

    public int[][] mapMatrix(Matrix matrix) {
        int n = matrix.getMatrixRows().size();
        int[][] result = new int[n][n];
        matrix.getMatrixRows()
                .forEach(row -> {
                    String[] valuesRaw = row.getRowValues().split(" ");
                    for (int i = 0; i < valuesRaw.length; i++) {
                        result[row.getRowNumber()][i] = Integer.parseInt(valuesRaw[i]);
                    }
                });
        return result;
    }

    public Matrix mapMatrix(int[][] matrix) {
        var result = new Matrix();
        result.setMatrixRaw(matrix);
        var rows = new ArrayList<MatrixRow>(matrix.length);
        result.setMatrixRows(rows);
        for (int i = 0; i < matrix.length; i++) {
            int[] row = matrix[i];
            var rowMapped = new MatrixRow();
            var rowValues = new StringBuilder();
            rowValues.append(row[0]);
            for (int j = 1; j < row.length; j++) {
                rowValues.append(' ');
                rowValues.append(row[j]);
            }
            rowMapped.setRowValues(rowValues.toString());
            rowMapped.setRowNumber(i);
            rowMapped.setMatrix(result);
            rows.add(rowMapped);
        }
        return result;
    }

}
