package net.edubovit.load.determinant.repository;

import net.edubovit.load.determinant.domain.Matrix;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MatrixRepository extends CrudRepository<Matrix, UUID> {

    Matrix findBySeed(long seed);

}
