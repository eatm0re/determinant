package net.edubovit.load.determinant.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;

@Data
@Entity
@Table(name = "matrix")
public class Matrix {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Basic
    @Column(name = "seed")
    private long seed;

    @Basic
    @Column(name = "determinant")
    private long determinant;

    @OneToMany(mappedBy = "matrix", cascade = ALL)
    private Collection<MatrixRow> matrixRows;

    @EqualsAndHashCode.Exclude
    private transient int[][] matrixRaw;

}
