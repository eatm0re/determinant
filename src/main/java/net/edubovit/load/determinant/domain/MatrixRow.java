package net.edubovit.load.determinant.domain;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@Table(name = "matrix_row")
public class MatrixRow {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Basic
    @Column(name = "row_number")
    public int getRowNumber() {
        return rowNumber;
    }
    private int rowNumber;

    @Basic
    @Column(name = "row_values")
    private String rowValues;

    @ManyToOne
    @JoinColumn(name = "matrix_id", referencedColumnName = "id", nullable = false)
    private Matrix matrix;

}
