package net.edubovit.load.determinant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeterminantApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeterminantApplication.class, args);
    }

}
