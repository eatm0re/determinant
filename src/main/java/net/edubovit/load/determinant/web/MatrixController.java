package net.edubovit.load.determinant.web;

import net.edubovit.load.determinant.service.MatrixService;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MatrixController {

    private final MatrixService matrixService;

    @GetMapping("{seed}/matrix")
    public int[][] getMatrixBySeed(@PathVariable long seed) {
        return matrixService.getMatrixBySeed(seed);
    }

    @GetMapping("{seed}/det")
    public long getDeterminantBySeed(@PathVariable long seed) {
        return matrixService.getDeterminantBySeed(seed);
    }

    @PostMapping("{seed}")
    public int[][] generateMatrixBySeed(@PathVariable long seed) {
        return matrixService.generateMatrixBySeed(seed);
    }

}
